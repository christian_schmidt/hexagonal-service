package de.tarent.ciwanzik.shoppingCart.ports.driven.database

import de.tarent.ciwanzik.shoppingCart.domain.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification
import spock.lang.Subject

@SpringBootTest
class JpaShoppingCartRepositoryTest extends Specification {

    private static def sku = new SKU("12345")
    private static def secondSku = new SKU("54321")

    @Autowired
    ShoppingCartJPARepository dbShoppingCartRepository

    @Subject
    JpaShoppingCartRepository domainShoppingCartRepository

    def setup() {
        domainShoppingCartRepository = new JpaShoppingCartRepository(dbShoppingCartRepository)
    }

    def "It can persist and load an empty shopping cart"() {
        given: "An empty shopping cart"
        ShoppingCart shoppingCart = anEmptyShoppingCart()
        ShoppingCartUuid shoppingCartUuid = shoppingCart.shoppingCartUuid

        when: "We persist it"
        domainShoppingCartRepository.save(shoppingCart)

        then: "We can also get it back"
        Optional<ShoppingCart> loadedShoppingCart = domainShoppingCartRepository.load(shoppingCartUuid)

        loadedShoppingCart.isPresent()
        loadedShoppingCart.get() == shoppingCart
    }

    def "It can persist and load an shopping carts with products"() {
        given: "A filled shopping cart"
        ShoppingCart shoppingCart = aFilledShoppingCart()
        ShoppingCartUuid shoppingCartUuid = shoppingCart.shoppingCartUuid

        when: "We persist it"
        domainShoppingCartRepository.save(shoppingCart)

        then: "We can also get it back with all products"
        Optional<ShoppingCart> loadedShoppingCart = domainShoppingCartRepository.load(shoppingCartUuid)

        loadedShoppingCart.isPresent()
        loadedShoppingCart.get() == shoppingCart
    }

    private ShoppingCart anEmptyShoppingCart() {
        return new ShoppingCart()
    }

    private ShoppingCart aFilledShoppingCart() {
        def shoppingCart = new ShoppingCart()
        shoppingCart.putProductInto(aProduct(), new Quantity(5))
        shoppingCart.putProductInto(anotherProduct(), new Quantity(10))

    }

    private Product aProduct(Price price = new Price(10, 00)) {
        return new Product(sku, price, new Name("Schokolade"))
    }

    private Product anotherProduct(Price price = new Price(10, 00)) {
        return new Product(secondSku, price, new Name("Brot"))
    }
}
